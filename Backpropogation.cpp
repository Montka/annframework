//
// Created by ujin on 28.06.16.
//

#include <typeinfo>
#include "Backpropogation.h"

void BackpropogationTeacher::trainNeuron(const TNeuron<double> *n, double delta_in) {
    teachCoeff = 0.01;
    double delta;
    double sum = 0;
    BaseNeuron *neuron;
    const DifferentableFunctor<double> *functor;
    try {
        neuron = const_cast<BaseNeuron *>(dynamic_cast<const BaseNeuron *>(n));
        if (neuron == nullptr) return;
        functor = dynamic_cast<const DifferentableFunctor<double> *>(neuron->transferFunctor);
        if (functor == nullptr) return;
    }
    catch (std::bad_cast) {
        return;
    }

    for (int i = 0; i < neuron->synapses.size(); ++i) {
        sum += neuron->synapses[i].out();
    }
    delta = functor->fi_(sum) * delta_in;
    neuron->threshold -= delta * teachCoeff;

    for (int i = 0; i < neuron->synapses.size(); ++i) {
        trainNeuron(neuron->synapses[i].input, delta * neuron->synapses[i].weight);
        neuron->synapses[i].weight -= teachCoeff * delta * neuron->synapses[i].signal();
    }

}


void BackpropogationTeacher::trainNetwork(Perceptron<double> *network, std::vector<double> in, std::vector<double> tg) {
    std::vector<double> out;
    TLayer<double> *outLayer = network->getOutputLayer();

    if (!network->setInputs(in)) return;
    out = network->calculate();
    for (int i = 0; i < outLayer->size(); ++i) {
        double delta = -(tg[i] - out[i]);
        trainNeuron(outLayer->at(i), delta);
    }
    network->invalidate();
}

