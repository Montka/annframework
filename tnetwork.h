//
// Created by ujin on 28.06.16.
//

#ifndef NEURALFRAMEWORK_TNETWORK_H
#define NEURALFRAMEWORK_TNETWORK_H

#include "tlayer.h"

template<typename T>
class TNetwork {
protected:
    std::vector<TLayer<T> *> layers;

public:
    template<typename A, typename... Params>
    bool createLayer(A ptr, Params... params) {
        try {
            if (dynamic_cast<TLayer<T> *>(&ptr) == nullptr) return false;
        }
        catch (std::bad_cast) {
            return false;
        }
        layers.push_back(new A(params...));
        return true;
    }

    void addLayer(TLayer<T> *ptr) {
        layers.push_back(ptr);
    }

    /*
     * First argument - where create synapse
     * Second - wither
     */
    void link(int layer1, int layer2, LinkerPredicate *predic) {
        for (int i = 0; i < layers[layer1]->size(); ++i) {
            for (int j = 0; j < layers[layer2]->size(); ++j) {
                if (predic->check(i, j)) {
                    layers[layer1]->linkNeuron(i, layers[layer2]->at(j));
                }

            }
        }
    };

    void linkAll(int layer1, int layer2) {
        for (int i = 0; i < layers[layer1]->size(); ++i)
            for (int j = 0; j < layers[layer2]->size(); ++j)
                layers[layer1]->linkNeuron(i, layers[layer2]->at(j));
    }

    virtual  ~TNetwork() {
        for (int i = 0; i < layers.size(); ++i) {
            delete (layers[i]);
        }
    }
};

#endif //NEURALFRAMEWORK_TNETWORK_H
