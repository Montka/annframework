//
// Created by ujin on 28.06.16.
//

#ifndef NEURALFRAMEWORK_PERCEPTRON_H
#define NEURALFRAMEWORK_PERCEPTRON_H

#include "tnetwork.h"
#include "BaseNeuron.h"

template<typename T>
class Perceptron : public TNetwork<T> {
    template<typename X>
    class InputNeuron : public TNeuron<X> {
    public:
        X output;

        virtual T out() const {
            return output;
        }
    };

    std::vector<InputNeuron<T> *> inputNeurons;
    std::vector<T> inputs;
    int outputLayer;

    template<typename F, typename...Params>
    void createBaseLayer(F fn, int count, Params...params) {
        TLayer<T> *lay = new TLayer<T>;
        this->addLayer(lay);
        lay->addNeurons(count, BaseNeuron(), fn);
        //lay->setWeights(new RandomWeight);

        createBaseLayer(fn, params...);
    }

    template<typename F>
    void createBaseLayer(F fn) {
        return;
    }


public:
    Perceptron() { }

    template<typename F, typename...Params>
    Perceptron(F fn, Params...params) {
        createBaseLayer(fn, params...);
        for (int i = 0; i < this->layers.size() - 1; ++i)
            this->linkAll(i + 1, i);

        setInputLayer(0);
        setOutputLayer(this->layers.size() - 1);

        for (auto l:this->layers)
            l->setWeights(new RandomWeight);

    }

    bool setInputLayer(unsigned int layer) {
        if (layer < 0 || layer >= this->layers.size())
            return false;

        for (int i = 0; i < this->layers[layer]->size(); ++i) {
            inputNeurons.push_back(new InputNeuron<T>);
            this->layers[layer]->linkNeuron(i, inputNeurons[i]);
            //layers[layer1]->linkNeuron(i,layers[layer2]->at(j));
        }
        return false;
    }

    void setOutputLayer(unsigned int layer) { outputLayer = layer; }

    bool setInputs(const std::vector<T> &vec) {
        if (vec.size() != inputNeurons.size())
            return false;
        for (int i = 0; i < inputNeurons.size(); ++i) {
            inputNeurons[i]->output = vec[i];
        }
        return true;
    }

    std::vector<T> calculate() {
        std::vector<T> out;
        for (int i = 0; i < this->layers[outputLayer]->size(); ++i) {
            out.push_back(this->layers[outputLayer]->at(i)->out());
            //std::cout<<this->layers[outputLayer]->at(i)->out()<<" ";
        }
        return out;
    }

    void invalidate() {
        for (auto l:this->layers) {
            for (int i = 0; i < l->neurons.size(); ++i) {
                dynamic_cast<BaseNeuron *>(l->neurons[i])->invalidate();
            }
        }
    }

    TLayer<double> *getOutputLayer() {
        return this->layers[outputLayer];
    }


};

#endif //NEURALFRAMEWORK_PERCEPTRON_H
