#include <iostream>
#include <vector>
#include <future>
#include <math.h>
#include "Functors.h"
#include "tnetwork.h"
#include "Perceptron.h"
#include "Backpropogation.h"

int GLOBAL_TIMES_OUT = 0;

//template <typename T>
//const TNeuron<T>* operator[](TLayer<T>* layer, int i)
//{
//    return layer->at(i);
//}
//class TestNeuron : public BaseNeuron {};



template<typename T>
class Teacher {
    //TNetwork<T> *network;
};


int main() {
    std::cout << "Hello, World!" << std::endl;
    srand(4);
    TFunctor<double> *ptr = new GiperbolicTgFunctor;
    int delta = 5;
    Perceptron<double> net(ptr, delta, 100, 1);
    std::vector<double> in{1, 1, 1, 1, 1};

    net.setInputs(in);
    std::vector<double> data;

    BackpropogationTeacher bp;

    double u = 1;
    for (double i = 0; i < 100; i += 0.01) {
        data.push_back(sin(i));
    }


    //std::cout<<std::vector<double>(&data[1],&data[2]).size();

    for (int i = 0; i < data.size() - delta - 1; ++i) {
        bp.trainNetwork(&net, std::vector<double>(&data[i], &data[i + delta]),
                        std::vector<double>(&data[i + delta], &data[i + delta + 1]));
    }

    for (int i = 0; i < data.size() - delta - 1; ++i) {
        net.setInputs(std::vector<double>(&data[i], &data[i + delta]));
        std::cout << net.calculate()[0] << " " << data[i + delta] << std::endl;

        net.invalidate();
    }
    std::cout << std::endl << GLOBAL_TIMES_OUT;
    return 0;
}