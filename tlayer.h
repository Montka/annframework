//
// Created by ujin on 28.06.16.
//

#ifndef NEURALFRAMEWORK_TLAYER_H
#define NEURALFRAMEWORK_TLAYER_H

#include "tneuron.h"
#include "Functors.h"

template<typename T>
class TLayer {
public:
    std::vector<TNeuron<T> *> neurons;

    template<typename A, typename... Params>
    bool addNeuron(A ptr, Params... params) {
        try {
            if (dynamic_cast<TNeuron<T> *>(&ptr) == nullptr) return false;
        }
        catch (std::bad_cast) {
            return false;
        }
        neurons.push_back(new A(params...));
        return true;
    }

    //template <typename A, typename...Params> bool addNeuron(std::shared_ptr<A> ptr, Params... params){ };


    template<typename... Params>
    bool addNeurons(int count, Params... params) {
        for (int i = 0; i < count; ++i) {
            if (!addNeuron(params...)) { return false; }
        }
        //std::cout<<neurons.size();
        return true;
    }

    const TNeuron<T> *at(int neuron) {
        return neurons[neuron];
    }

    bool linkNeuron(int neuron1, const TNeuron<T> *neuron2) {
        if (neuron1 < 0 || neuron1 >= neurons.size())
            return false;

        neurons[neuron1]->addSynapse(neuron2);
        return true;
    }

    unsigned int size() const {
        return neurons.size();
    }

    void setWeights(WeightFunctor<T> *func) {
        for (int i = 0; i < neurons.size(); ++i)
            for (int j = 0; j < neurons[i]->synapses.size(); ++j) {
                neurons[i]->synapses[j].weight = func->set(i, j);
            }
        delete (func);

    }

    TLayer() = default;

    virtual ~TLayer() {
        for (int i = 0; i < neurons.size(); ++i) {
            delete (neurons[i]);
        }
    }

};

#endif //NEURALFRAMEWORK_TLAYER_H
