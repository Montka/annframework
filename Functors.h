//
// Created by ujin on 28.06.16.
//

#ifndef NEURALFRAMEWORK_FUNCTORS_H
#define NEURALFRAMEWORK_FUNCTORS_H

#include <math.h>
#include "stdlib.h"

template<typename T>
class TFunctor {
public:
    virtual T fi(T) const = 0;
};

template<typename T>
class DifferentableFunctor : public TFunctor<T> {
public:
    virtual T fi_(T) const = 0;
};

class LinearFunctor : public DifferentableFunctor<double> {
public:
    virtual double fi(double arg) const {
        return arg;
    }

    virtual double fi_(double arg) const {
        return 1;
    }
};

class SigmoidFunctor : public DifferentableFunctor<double> {
public:
    virtual double fi(double arg) const {
        return 1 / (1 + exp(-arg));
    }

    virtual double fi_(double arg) const {
        return exp(-arg) / pow((1 + exp(-arg)), 2);
    }
};

class ThresholdFunctor : public TFunctor<double> {
public:
    virtual double fi(double arg) const {
        return (arg < 0 ? 0 : 1);
    }
};

class SignFunctor : public TFunctor<double> {
public:
    virtual double fi(double arg) const {
        return (arg < 0 ? -1 : 1);
    }
};

class GiperbolicTgFunctor : public DifferentableFunctor<double> {
public:
    virtual double fi(double arg) const {
        return (exp(arg) - exp(-arg)) / (exp(arg) + exp(-arg));
    }

    virtual double fi_(double arg) const {
        return 1 - (pow(-exp(-arg) + exp(arg), 2) / pow(exp(-arg) + exp(arg), 2));
    }
};

class RadialBasicFunctor : public DifferentableFunctor<double> {
public:
    virtual double fi(double arg) const {
        return (exp(-pow(arg, 2)));
    }

    virtual double fi_(double arg) const {
        return -2 * exp(-pow(arg, 2)) * arg;
    }
};

template<typename T>
class WeightFunctor {
public:
    virtual T set(int, int) = 0;
};

class RandomWeight : public WeightFunctor<double> {
public:
    virtual double set(int n, int s) {
        return (rand() % 100 - 50) / 100.;
    }
};

class LinkerPredicate {
public:
    virtual bool check(int, int) = 0;
};

class OneToOneLinker : public LinkerPredicate {
public:
    virtual bool check(int a, int b) const {
        return a == b;
    }
};

#endif //NEURALFRAMEWORK_FUNCTORS_H
