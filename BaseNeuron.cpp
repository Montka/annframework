//
// Created by ujin on 28.06.16.
//

#include "BaseNeuron.h"

double BaseNeuron::out() const {
    if (!outCached) {
        double sum = 0;
        for (int i = 0; i < synapses.size(); ++i) {
            sum += synapses[i].out();
            //std::cout<<sum<<" ";
        }
        sum += threshold;
        output = transferFunctor->fi(sum);
        outCached = true;
    }
    return output;
}

void BaseNeuron::invalidate() {
    outCached = false;
}

BaseNeuron::BaseNeuron(const TFunctor<double> *tF) :
        transferFunctor(tF),
        output(0),
        threshold(0),
        outCached(false) { }

