//
// Created by ujin on 28.06.16.
//

#ifndef NEURALFRAMEWORK_BASENEURON_H
#define NEURALFRAMEWORK_BASENEURON_H

#include "tneuron.h"
#include "Functors.h"

class BaseNeuron : public TNeuron<double> {
protected:
    mutable double output;

    mutable bool outCached;


public:
    mutable double threshold;
    const TFunctor<double> *transferFunctor;

    double out() const;

    void invalidate();

    BaseNeuron(const TFunctor<double> *tF);

    BaseNeuron() = default;
};

#endif //NEURALFRAMEWORK_BASENEURON_H
