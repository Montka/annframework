//
// Created by ujin on 28.06.16.
//

#ifndef NEURALFRAMEWORK_TNEURON_H
#define NEURALFRAMEWORK_TNEURON_H

#include <vector>

template<typename T>
class TNeuron {
public:
    template<typename X>
    struct Synapse {
        Synapse(const TNeuron<X> *i, double w) : input(i), weight(w) { }

        const TNeuron<X> *input;
        double weight;

        X out() const {
            return input->out() * weight;
        }

        X signal() const {
            return input->out();
        }
    };

    std::vector<Synapse<T>> synapses;

    virtual bool addSynapse(const TNeuron<T> *in, int w = 1) {
        for (int i = 0; i < synapses.size(); ++i) {
            if (in == synapses[i].input) return false;
        }
        synapses.emplace_back(in, w);
        return true;
    }

    virtual T out() const = 0;

    TNeuron() = default;
};

#endif //NEURALFRAMEWORK_TNEURON_H
