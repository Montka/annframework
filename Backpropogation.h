//
// Created by ujin on 28.06.16.
//

#ifndef NEURALFRAMEWORK_BACKPROPOGATION_H
#define NEURALFRAMEWORK_BACKPROPOGATION_H


#include "Perceptron.h"

class BackpropogationTeacher /*:public Teacher<double> */{
    double teachCoeff;

    void trainNeuron(const TNeuron<double> *n, double delta_in);

public:
    void trainNetwork(Perceptron<double> *network, std::vector<double, std::allocator<double>> in,
                      std::vector<double, std::allocator<double>> tg);


};


#endif //NEURALFRAMEWORK_BACKPROPOGATION_H
